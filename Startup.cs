using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Prometheus;
using Microsoft.Extensions.Options;
using SwP.Fulfillment.ChameleoonProxy.Services;

namespace SwP.Fulfillment.ChameleoonProxy
{
    public class Startup
    {

        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"customSettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();


            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();

            //Metrics
            services.AddHttpClient(Options.DefaultName).UseHttpClientMetrics();
            services.AddHealthChecks()
                // ...
                //.AddCheck<DummyHealthCheck>(nameof(DummyHealthCheck))
                //.AddCheck<DummyUnHealthCheck>(nameof(DummyUnHealthCheck))
                .AddCheck<AbraHealthCheck>(nameof(AbraHealthCheck))
            // ...
            .ForwardToPrometheus();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Fulfillment.Customers.API", Version = "v1.0" });
                c.AddSecurityDefinition("basic", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "basic",
                    In = ParameterLocation.Header,
                    Description = "Basic Authorization header using the Bearer scheme."
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "basic"
                            }
                        },
                        System.Array.Empty<string>()
                    }
                });
            });
            // configure basic authentication 
            services.AddAuthentication("BasicAuthentication")
                    .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);


            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<IChameleoonService, ChameleoonService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())// || env.IsProduction())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SwP.Fulfillment.Chameleoon.API v3.0"));
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseHttpMetrics(options =>
            {
                // This will preserve only the first digit of the status code.
                // For example: 200, 201, 203 -> 2xx
                options.ReduceStatusCodeCardinality();
                options.AddCustomLabel("host", context => context.Request.Host.Host);
            });


            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapMetrics();//.RequireAuthorization();

                endpoints.MapControllers();
            });
        }
    }
}
