﻿using System.Collections.Generic;
using System;

namespace SwP.Fulfillment.ChameleoonProxy
{
    public class KeyLengthSortedDictionary : SortedDictionary<string, string>
    {
        private class StringLengthComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                if (x == null) throw new ArgumentNullException(nameof(x));
                if (y == null) throw new ArgumentNullException(nameof(y));
                var lengthComparison = x.Length.CompareTo(y.Length);
                return lengthComparison == 0 ? string.Compare(x, y, StringComparison.Ordinal) : lengthComparison;
            }
        }

        public KeyLengthSortedDictionary() : base(new StringLengthComparer())
        {
        }
    }

    public class KeyLengthSortedDescendingDictionary : SortedDictionary<string, string>
    {
        private class StringLengthComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                if (x == null) throw new ArgumentNullException(nameof(x));
                if (y == null) throw new ArgumentNullException(nameof(y));
                var lengthComparison = x.Length.CompareTo(y.Length) * -1;
                return lengthComparison == 0 ? string.Compare(x, y, StringComparison.Ordinal) : lengthComparison;
            }
        }

        public KeyLengthSortedDescendingDictionary() : base(new StringLengthComparer())
        {
        }
    }
}
