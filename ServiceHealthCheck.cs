﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using System.Net.Http.Headers;
using SwP.Fulfillment.ChameleoonProxy.Models;

namespace SwP.Fulfillment.ChameleoonProxy
{
    public class DummyHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var isHealthy = true;

            // ...

            if (isHealthy)
            {
                return Task.FromResult(
                    HealthCheckResult.Healthy("A healthy result."));
            }

            return Task.FromResult(
                new HealthCheckResult(
                    context.Registration.FailureStatus, "An unhealthy result."));
        }
    }
    public class DummyUnHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context, CancellationToken cancellationToken = default)
        {

            return Task.FromResult(
                new HealthCheckResult(
                    context.Registration.FailureStatus, "An unhealthy result."));
        }
    }

    public class AbraHealthCheck : IHealthCheck
    {
        private readonly IConfiguration _config;

        public AbraHealthCheck(IConfiguration config)
            => _config = config;
        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var baseAddress = _config.GetValue<string>("BaseAddress");
            var user = _config.GetSection("SrcApiUser").Get<User>();
            var plainText = System.Text.Encoding.UTF8.GetBytes($"{user.Username}:{user.Password}");

            using (HttpClient client = new() { BaseAddress = new Uri(baseAddress), })
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(plainText));
                var requestUri = "firms?take=1&select=id";
                try
                {
                    var response = await client.GetAsync(requestUri);

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception("Url not responding with 200 OK");
                    }
                }
                catch (Exception)
                {
                    return await Task.FromResult(HealthCheckResult.Unhealthy());
                }
            }


            return await Task.FromResult(HealthCheckResult.Healthy());

        }
    }
    public class ChameleoonHealthCheck : IHealthCheck
    {
        private readonly IConfiguration _config;

        public ChameleoonHealthCheck(IConfiguration config)
            => _config = config;
        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var baseAddress = _config.GetValue<string>("BaseAddress");
            var user = _config.GetSection("SrcApiUser").Get<User>();
            var plainText = System.Text.Encoding.UTF8.GetBytes($"{user.Username}:{user.Password}");

            using (HttpClient client = new() { BaseAddress = new Uri(baseAddress), })
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(plainText));
                var requestUri = "firms?take=1&select=id";
                try
                {
                    var response = await client.GetAsync(requestUri);

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception("Url not responding with 200 OK");
                    }
                }
                catch (Exception)
                {
                    return await Task.FromResult(HealthCheckResult.Unhealthy());
                }
            }


            return await Task.FromResult(HealthCheckResult.Healthy());

        }
    }
}

