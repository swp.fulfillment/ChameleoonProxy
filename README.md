# Chameleoon Proxy
Created to allow integration of Chameleoon service into Abra WMS.

Relays order informations from WMS to Chameleoon and status updates from Chameleoon to WMS.
