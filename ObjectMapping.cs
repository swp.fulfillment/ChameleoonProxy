﻿using SwP.Fulfillment.ChameleoonProxy.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SwP.Fulfillment.ChameleoonProxy
{
    public static class ObjectMapping
    {
        private static string GetMatchingProperty<T>(string propertyName)
        {
            return typeof(T).GetProperty(propertyName)
                .GetCustomAttributes(typeof(AbraWebApiFieldNameAttribute), true)
                .Cast<AbraWebApiFieldNameAttribute>()
                .FirstOrDefault().PropertyName;
        }

        private static string GetMatchingObject<T>(string objectName)
        {
            return typeof(T)
                .GetCustomAttributes(typeof(AbraWebApiObjectNameAttribute), true)
                .Cast<AbraWebApiObjectNameAttribute>()
                .FirstOrDefault().ObjectName;
        }


        public static U Map<T, U>(this T t, U u)
        {
            var map = JsonToPropertyNamesMap<T>();

            foreach (var uProp in typeof(U).GetProperties())
            {
                var uPropAtt = uProp.GetCustomAttributes(typeof(AbraWebApiFieldNameAttribute), true);
                var uPropName = uPropAtt?.Cast<AbraWebApiFieldNameAttribute>().FirstOrDefault()?.PropertyName;
                if (uPropName != null)
                {
                    var tPropName = map[uPropName];
                    var tProp = typeof(T).GetProperty(tPropName);
                    if (tProp != null)
                    {
                        MapPropertyValue(t, u, uProp, tProp);
                    }
                }
            }

            return u;
        }

        public static Dictionary<string, string> JsonToPropertyNamesMap<T>()
        {
            var map = new Dictionary<string, string>();
            foreach (var property in typeof(T).GetProperties())
            {
                var jsonAttributes = property.GetCustomAttributes(typeof(Newtonsoft.Json.JsonPropertyAttribute), true);
                if (jsonAttributes != null)
                {
                    var jsonPropertyName = ((Newtonsoft.Json.JsonPropertyAttribute)jsonAttributes.First()).PropertyName;
                    map.Add(jsonPropertyName, property.Name);
                }
            }
            return map;
        }

        public static U ReverseMap<T, U>(this T t, U u)
        {
            foreach (var uProp in typeof(U).GetProperties())
            {
                var uPropAtt = uProp.GetCustomAttributes(typeof(AbraWebApiFieldNameAttribute), true);
                var uPropName = uPropAtt?.Cast<AbraWebApiFieldNameAttribute>().FirstOrDefault()?.PropertyName;
                if (uPropName != null)
                {
                    var tProp = typeof(T).GetProperty(uPropName);
                    if (tProp != null && tProp.PropertyType == uProp.PropertyType)
                    {
                        //var uPropValue = uProp.GetValue(u);
                        //tProp.SetValue(t, uPropValue);
                        MapPropertyValue(u, t, tProp, uProp);
                    }
                }
            }
            return u;
        }

        public static IEnumerable<string> GetJsonPropertyNames<T>()
        {
            var list = new List<string>();
            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var propertyAttribute = propertyInfo.GetCustomAttributes(typeof(AbraWebApiFieldNameAttribute), true);
                var propertyName = propertyAttribute?.Cast<AbraWebApiFieldNameAttribute>().FirstOrDefault()?.PropertyName;
                if (propertyName != null)
                {
                    list.Add(propertyName);
                }
            }
            return list;
        }

        public static IEnumerable<string> GetNewtonsoftJsonPropertyNames<T>()
        {
            var list = new List<string>();
            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var propertyAttribute = propertyInfo.GetCustomAttributes(typeof(Newtonsoft.Json.JsonPropertyAttribute), true);
                if (propertyAttribute != null)
                {
                    var jsonPropName = ((Newtonsoft.Json.JsonPropertyAttribute)propertyAttribute.First()).PropertyName;
                    list.Add(jsonPropName);

                }
            }
            return list;
        }
        private static void MapPropertyValue<T, U>(T t, U u, PropertyInfo uProp, PropertyInfo tProp)
        {
            var tPropValue = tProp.GetValue(t);
            if (tProp.PropertyType == uProp.PropertyType)
            {
                uProp.SetValue(u, tPropValue);
            }
            else if (tProp.PropertyType == typeof(string) && uProp.PropertyType == typeof(int))
            {
                //int parse from string
                var intValue = int.Parse((string)tPropValue);
                uProp.SetValue(u, intValue);
            }
            else if (tProp.PropertyType == typeof(int) && uProp.PropertyType == typeof(string))
            {
                uProp.SetValue(u, tPropValue.ToString());
            }
            else if (tProp.PropertyType == typeof(string) && uProp.PropertyType == typeof(string[]))
            {
                //string[] parse from string
                var tokens = ((string)tPropValue).Split(';');
                uProp.SetValue(u, tokens);
            }
            else if (tProp.PropertyType == typeof(string[]) && uProp.PropertyType == typeof(string))
            {
                uProp.SetValue(u, string.Join(';', (string[])tPropValue));
            }
        }
    }
}
