﻿using System;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    internal class AbraWebApiObjectNameAttribute : Attribute
    {
        public AbraWebApiObjectNameAttribute(string objectName)
        {
            ObjectName = objectName;
        }

        public string ObjectName { get; }
    }
}