﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class AbraReceivedorderMinimal
    {
        /// <summary>
        /// Typ dopr.; ID objektu Spôsob dopravy [perzistentná položka]
        /// </summary>
        /// <value>Typ dopr.; ID objektu Spôsob dopravy [perzistentná položka]</value>
        [DataMember(Name = "TransportationType_ID.Code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "TransportationType_ID.Code")]
        public string TransportationTypeID_Code { get; set; }

        ///// <summary>
        ///// Reference URL [perzistentná položka]
        ///// </summary>
        ///// <value>Reference URL [perzistentná položka]</value>
        //[DataMember(Name = "X_URL", EmitDefaultValue = false)]
        //[JsonProperty(PropertyName = "X_URL")]
        //public string X_URL { get; set; }

        /// <summary>
        /// Status objednávky; ID objektu Status objednávky [perzistentná položka]
        /// </summary>
        /// <value>Status objednávky; ID objektu Status objednávky [perzistentná položka]</value>
        [DataMember(Name = "X_STATUS_OBJEDNAVKY_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_STATUS_OBJEDNAVKY_ID")]
        public string X_STATUS_OBJEDNAVKY_ID { get; set; }

        /// <summary>
        /// Tracking ID objednávky [perzistentná položka]
        /// </summary>
        /// <value>Tracking ID objednávky [perzistentná položka]</value>
        [DataMember(Name = "X_TRACKING_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_TRACKING_ID")]
        public string X_TRACKING_ID { get; set; }
    }
}