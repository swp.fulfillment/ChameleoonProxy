﻿using System.Linq;

namespace SwP.Fulfillment.ChameleoonProxy.Models.Extensions
{
    public static class ChameleoonOrderStatusUpdateExtension
    {
        public static AbraReceivedorderMinimal ToAbraReceivedorderSimplified(this ChameleoonOrderStatusUpdate orState)
        {
            var ars = new AbraReceivedorderMinimal
            {
                //ars.TransportationTypeID_Code = orState.Courier;
                X_STATUS_OBJEDNAVKY_ID = orState.TargetState.ToCode(),
                X_TRACKING_ID = orState.ShippingNumber
            };
            if (orState.AdditionalShippingNumbers != null && orState.AdditionalShippingNumbers.Any())
            {
                ars.X_TRACKING_ID += ", " + string.Join(", ", orState.AdditionalShippingNumbers);
            }
            return ars;
        }
    }
}
