﻿namespace SwP.Fulfillment.ChameleoonProxy.Models.Extensions
{
    public static class OrderStatusExtension
    {
        public static string ToCode(this OrderStatus o)
        {
            return o switch
            {
                OrderStatus.Inserted => "1170000101",
                OrderStatus.Received => "2170000101",
                OrderStatus.Processing => "3170000101",
                OrderStatus.WaitingForPickup => "4170000101",
                OrderStatus.Sent => "1180000101",
                OrderStatus.Shipped_NotTracked => "21E1000101",
                OrderStatus.Delivered => "2180000101",
                OrderStatus.Rejected => "3180000101",
                OrderStatus.Cancelled => "4180000101",
                OrderStatus.BackOrder => "5180000101",
                OrderStatus.Returned => "11I0000101",
                OrderStatus.Duplicate => "11U0000101",
                OrderStatus.PartialyReturned => "21W0000101",
                OrderStatus.PickedUp_OnDelivery => "1121000101",
                OrderStatus.Undelivered => "2121000101",
                OrderStatus.Returning => "3121000101",
                OrderStatus.WaitingForCollection => "4121000101",
                OrderStatus.Investigation => "2171000101",
                OrderStatus.Lost => "3171000101",
                OrderStatus.Picking => "41A1000101",
                OrderStatus.Packing => "31A1000101",   //formerly "41A1000101" ... status codes form picking and packing were swapped
                _ => "null",
            };
            //"02","2170000101","Received"
            //"03","3170000101","Processing"
            //"04","4170000101","Waiting for pickup"
            //"05","1180000101","Sent"
            //"06","2180000101","Delivered"
            //"07","3180000101","Rejected"
            //"08","4180000101","Cancelled"
            //"09","5180000101","Backorder"
            //"10","11I0000101","Returned"
            //"99","11U0000101","Duplicate"
            //"11","21W0000101","Partially returned"
            //"21","1121000101","Picked up - On delivery"
            //"22","2121000101","Undelivered"
            //"23","3121000101","Returning"
            //"24","4121000101","Waiting for collection"
            //"25","2171000101","Investigation"
            //"27","3171000101","Lost"
            //"32","31A1000101","Packing"
            //"31","41A1000101","Picking"
        }

        public static OrderStatus FromCode(this string code)
        {
            return code switch
            {
                "1170000101" => OrderStatus.Inserted,
                "2170000101" => OrderStatus.Received,
                "3170000101" => OrderStatus.Processing,
                "4170000101" => OrderStatus.WaitingForPickup,
                "1180000101" => OrderStatus.Sent,
                "2180000101" => OrderStatus.Delivered,
                "3180000101" => OrderStatus.Rejected,
                "4180000101" => OrderStatus.Cancelled,
                "5180000101" => OrderStatus.BackOrder,//return OrderStatus.Postponed;
                "11I0000101" => OrderStatus.Returned,
                "11U0000101" => OrderStatus.Duplicate,
                "21W0000101" => OrderStatus.PartialyReturned,
                "1121000101" => OrderStatus.PickedUp_OnDelivery,
                "2121000101" => OrderStatus.Undelivered,
                "3121000101" => OrderStatus.Returning,
                "4121000101" => OrderStatus.WaitingForCollection,
                "2171000101" => OrderStatus.Investigation,
                "3171000101" => OrderStatus.Lost,
                "31A1000101" => OrderStatus.Picking,
                "41A1000101" => OrderStatus.Packing,
                _ => OrderStatus.ErrorValue,
            };
        }
    }
}
