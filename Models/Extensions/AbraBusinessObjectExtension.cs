﻿namespace SwP.Fulfillment.ChameleoonProxy.Models.Extensions
{
    public static class AbraBusinessObjectExtension
    {
        public static string ToNullIfEmpty(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;
            return value;
        }

        /// <summary>
        /// Convert WMS country code to ISO code
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static string CountryIDToIsoCode(this string countryCode)
        {
            if (string.IsNullOrWhiteSpace(countryCode))
                return null;

            var countryIso = countryCode.Replace("00000", "");
            countryIso = countryIso.Replace("000", "");
            switch (countryIso)
            {
                case "00":
                    return null;
                case "10GB":
                case "20GB":
                    return "GB";
                default:
                    if (countryIso.Length == 2)
                        return countryIso;
                    else
                        return null;
            }
        }

        /// <summary>
        /// Attempt to partially fix legacy mess in WMS
        /// Originally "country" was a freetext field and customers could write anything there
        /// This is and attempt to automatically fix at least the most common cases.
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public static string CountryToIsoCode(this string country)
        {
            if (string.IsNullOrWhiteSpace(country))
                return "SK";
            switch (country.ToLowerInvariant())
            {
                case "slovakia":
                case "slovensko":
                case "sr":
                case "slovak republic":
                case "slovenska republika":
                case "slovenská republika":
                    return "SK";

                case "czechia":
                case "cesko":
                case "cr":
                case "czech republic":
                case "ceska republika":
                case "česká republika":
                    return "CZ";

                case "hungaria":
                case "hungary":
                case "madarsko":
                case "maďarsko":
                    return "HU";

                case "aurstria":
                case "rakusko":
                case "rakousko":
                case "osterreich":
                    return "AT";
            }
            return country.ToUpperInvariant();
        }
    }


}
