﻿using System.Collections.Generic;
using System;

namespace SwP.Fulfillment.ChameleoonProxy.Models.Extensions
{
    public static class ChameleoonOrderExtension
    {
        public static ChameleoonOrder FromAbraReceivedorder(this ChameleoonOrder chorder, AbraReceivedorder aorder)
        {
            //var chorder = new ChameleoonOrder();

            chorder.Addressee           = aorder.XB2CFirstName + " " + aorder.XB2CLastName;
            chorder.OrganizationName    = aorder.XB2CFirm.ToNullIfEmpty();
            chorder.Street              = aorder.XB2CStreet.ToNullIfEmpty();
            chorder.Street2             = aorder.XAddressLine2.ToNullIfEmpty();
            chorder.City                = aorder.XB2CCity.ToNullIfEmpty();
            chorder.ZipCode             = aorder.XB2CPostCode;
            chorder.CountryCode         = aorder.XCountryIsoCode.CountryIDToIsoCode() ?? aorder.XB2CCountry.CountryToIsoCode(); //if is country_iso_code empty use XB2CCountry 
            chorder.Phone               = aorder.XB2CPhoneNumber;
            chorder.Email               = aorder.XB2CEMail;
            chorder.Note                = aorder.XZakaznikPoznamkaZwww.ToNullIfEmpty();
            chorder.ReferenceNumber     = aorder.ID;// aorder.DisplayName;
            chorder.CourierService      = aorder.TransportationTypeID_Code;
            if (aorder.Weight != null)
                chorder.Weight          = (decimal)aorder.Weight;
            chorder.PackageCount        = null;
            chorder.CodPrice            = null;// 0M;
            chorder.StateId             = aorder.X_STATUS_OBJEDNAVKY_ID;
            chorder.CreateDateTime      = (DateTime)aorder.CreatedAtDATE;

            //dpd sk is specific. It throws error if publicPackagePoint is filled
            if (chorder.CourierService.ToLowerInvariant() != "dpdsk")
            {
                //Custom logic to handle Zasielkovna variants in Abra
                chorder.PublicPackagePoint = aorder.X_PICKUP_POINT.ToNullIfEmpty();
                if (chorder.CourierService.ToLowerInvariant() != "zpoint" && string.IsNullOrWhiteSpace(chorder.PublicPackagePoint))
                {
                    chorder.PublicPackagePoint = chorder.CourierService;
                }
            }

            chorder.IsBackShipping      = false;
            chorder.OrderedShippingDate = null;
            chorder.Currency            = aorder.CurrencyID_Code;
            chorder.DriverNote          = null;
            chorder.ContactPerson       = null;
            chorder.VariableSymbol      = aorder.X_ZAKAZNICKE_CISLO_OP.ToNullIfEmpty();
            chorder.Description         = null;
            chorder.DocumentType        = ChameleoonDocType.Order;
            chorder.PaymentType         = aorder.PaymentTypeID_Code;
            chorder.TotalPrice          = 0M;

            var items = new List<ChameleoonItem>();
            if (aorder.Rows != null)
                foreach (var row in aorder.Rows)
                {
                    if (row.Text.ToLower() == "hodnotatovaru" || row.Text.ToLower() == "hodnota tovaru")
                    {
                        chorder.TotalPrice = (decimal)row.TotalPrice;
                        continue;
                    }

                    if (row.Text.ToLower() == "výškadobierky" || row.Text.ToLower() == "výška dobierky" || row.Text.ToLower() == "vyskadobierky" || row.Text.ToLower() == "vyska dobierky")
                    {
                        chorder.CodPrice = (decimal?)row.TotalPrice;
                        continue;
                    }

                    var item = new ChameleoonItem
                    {
                        Id              = row.ID,
                        Code            = row.StoreCardID_Code.ToNullIfEmpty(),
                        EanCode         = row.StoreCardID_Ean.ToNullIfEmpty(),
                        Name            = row.StoreCardID_DisplayName,
                        Quantity        = (decimal)row.UnitQuantity,
                        MeasureUnit     = row.QUnit.ToNullIfEmpty(),
                        Text            = null
                    };
                    items.Add(item);
                }
            chorder.Items               = items.ToArray();
            return chorder;
        }
    }
}
