﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    public class QueryParameters
    {
        //WHERE
        [Description("WHERE condition similar to SQL")]
        public string Where { get; set; }

        //SELECT
        //public string[] Select { get; set; }
        //EXPAND
        //public QueryParametersExpandObject Expand { get; set; }
        //GROUPBY
        public string[] GroupBy { get; set; }

        //ORDERBY
        public string[] OrderBy { get; set; }

        //SKIP
        public int Skip { get; set; }

        //TAKE
        public int Take { get; set; }



        /// <summary>
        /// Compose query with fields based on provided object
        /// </summary>
        /// <typeparam name="T">Main object</typeparam>
        /// <returns></returns>
        public string ComposeQuery<T>()
        {
            string query = string.Empty;
            if (!string.IsNullOrWhiteSpace(Where))
            {
                query = "where=" + QueryParser<T>() + "&";
            }
            if (GroupBy != null && GroupBy.Any())
            {
                query += "groupby=(" + string.Join(',', GroupBy) + ")&";
            }
            if (OrderBy != null && OrderBy.Any())
            {
                query += "orderby=(" + string.Join(',', OrderBy) + ")&";
            }
            if (Skip > 0)
            {
                query += "skip=" + Skip + "&";
            }
            if (Take > 0)
            {
                query += "take=" + Take + "&";
            }
            return $"{query}{ComposeSelect<T>()}";
        }

        /// <summary>
        /// Compose query with fields based on provided object
        /// </summary>
        /// <typeparam name="T">Main object</typeparam>
        /// <typeparam name="U">Expand object</typeparam>
        /// <returns></returns>
        public string ComposeQuery<T, U>()
        {
            string query = string.Empty;
            if (!string.IsNullOrWhiteSpace(Where))
            {
                query = "where=" + QueryParser<T>() + "&";
            }
            if (GroupBy != null && GroupBy.Any())
            {
                query += "groupby=(" + string.Join(',', GroupBy) + ")&";
            }
            if (OrderBy != null && OrderBy.Any())
            {
                query += "orderby=(" + string.Join(',', OrderBy) + ")&";
            }
            if (Skip > 0)
            {
                query += "skip=" + Skip + "&";
            }
            if (Take > 0)
            {
                query += "take=" + Take + "&";
            }
            return $"{query}{ComposeSelectWithRows<T, U>()}";
        }

        public string ComposeCountQuery<T>()
        {
            string query = string.Empty;
            if (!string.IsNullOrWhiteSpace(Where))
            {
                query = "where=" + QueryParser<T>() + "&";
            }
            if (GroupBy != null && GroupBy.Any())
            {
                query += "groupby=(" + string.Join(',', GroupBy) + ")&";
            }
            if (OrderBy != null && OrderBy.Any())
            {
                query += "orderby=(" + string.Join(',', OrderBy) + ")&";
            }
            if (Skip > 0)
            {
                query += "skip=" + Skip + "&";
            }
            if (Take > 0)
            {
                query += "take=" + Take + "&";
            }

            return query + "select=Count(ID)";
        }

        /// <summary>
        /// Compose select with field from provided object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string ComposeSelect<T>()
        {
            var jsonPropAttNames = ObjectMapping.GetJsonPropertyNames<T>();
            jsonPropAttNames = jsonPropAttNames.Except(new[] { "Rows" });
            return "select=" + string.Join(',', jsonPropAttNames.ToArray());
        }
        public static string ComposeSelectFromAbraObject<T>()
        {
            var jsonPropAttNames = ObjectMapping.GetNewtonsoftJsonPropertyNames<T>();
            jsonPropAttNames = jsonPropAttNames.Except(new[] { "Rows" });
            return "select=" + string.Join(',', jsonPropAttNames.ToArray());
        }

        /// <summary>
        /// Compose select with field from provided object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string ComposeSelectWithRows<T, U>()
        {
            var jsonPropAttNames = ObjectMapping.GetJsonPropertyNames<T>();
            jsonPropAttNames = jsonPropAttNames.Except(new[] { "Rows" });
            var jsonPropAttNames2 = ObjectMapping.GetJsonPropertyNames<U>();
            return $"select={string.Join(',', jsonPropAttNames.ToArray())}&expand=Rows({string.Join(',', jsonPropAttNames2.ToArray())})";
        }
        public static string ComposeSelectWithRowsFromAbraObject<T, U>()
        {
            var jsonPropAttNames = ObjectMapping.GetNewtonsoftJsonPropertyNames<T>();
            jsonPropAttNames = jsonPropAttNames.Except(new[] { "Rows" });
            var jsonPropAttNames2 = ObjectMapping.GetNewtonsoftJsonPropertyNames<U>();
            return $"select={string.Join(',', jsonPropAttNames.ToArray())}&expand=Rows({string.Join(',', jsonPropAttNames2.ToArray())})";
        }

        private string QueryParser<T>()
        {
            if (string.IsNullOrWhiteSpace(Where)) return Where;
            var result = Where.ToLowerInvariant();
            var operators = new Dictionary<string, string>() {
                { "!=", "+ne+" },
                { "<=", "+le+" },
                { ">=", "+ge+" },
                { "<>", "+ne+" },
                { "||", "+or+" },
                { "&&", "+and+" },
                { "=", "+eq+" },
                { "<", "+lt+" },
                { ">", "+gt+" } };
            var mappings = GetMappingDictionary<T>();
            foreach (var prop in mappings)
            {
                result = result.Replace(prop.Key.ToLower(), prop.Value);
            }
            foreach (var op in operators)
            {
                result = result.Replace(op.Key, op.Value);
            }
            return result.ToUpperInvariant();
        }

        private static KeyLengthSortedDescendingDictionary GetMappingDictionary<T>()
        {
            var dict = new KeyLengthSortedDescendingDictionary();
            foreach (var tProp in typeof(T).GetProperties())
            {
                var tPropAtt = tProp.GetCustomAttributes(typeof(AbraWebApiFieldNameAttribute), true);
                var tPropName = tPropAtt?.Cast<AbraWebApiFieldNameAttribute>().FirstOrDefault()?.PropertyName;
                if (tPropName != null)
                {
                    dict.Add(tProp.Name, tPropName);
                }
            }
            return dict;
        }

    }
}
