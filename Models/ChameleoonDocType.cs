﻿namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    public enum ChameleoonDocType
    {
        Order,
        Invoice,
        Issue
    }
}
