﻿using System.ComponentModel.DataAnnotations;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
