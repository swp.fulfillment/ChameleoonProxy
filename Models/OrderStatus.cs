﻿using System.Text.Json.Serialization;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum OrderStatus
    {
        Inserted = 1,              // 1170000101
        Received = 2,              // 2170000101
        Processing = 3,            // 3170000101
        WaitingForPickup = 4,      // 4170000101
        Sent = 5,	               // 1180000101
        Delivered = 6,	           // 2180000101 (renamed from closed)
        Rejected = 7,	           // 3180000101
        Cancelled = 8,	           // 4180000101
        BackOrder = 9, 	           // 5180000101 (renamed from postponed)
        Returned = 10,	           // 11I0000101
        PartialyReturned = 11,     // 21W0000101
        PickedUp_OnDelivery = 21,  // 1121000101 Picked up - On delivery;  
        Undelivered = 22,          // 2121000101 Undelivered
        Returning = 23,            // 3121000101 Returning; 
        WaitingForCollection = 24, // 4121000101 Waiting for collection
        Investigation = 25,          // 2171000101 code 25
        Lost = 27,                   // 3171000101
        Picking = 31,                // 31A1000101  !important note Picking and Packing was swapped
        Packing = 32,              // 41A1000101
        Duplicate = 99,            // 11U0000101
        Shipped_NotTracked = 61,   // 21E1000101
        ErrorValue = 13,
    }
}

