﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class AbraReceivedorderrow
    {

        [DataMember(Name = "rowtype", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "rowtype")]
        public int RowType { get; set; }

        [DataMember(Name = "store_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "store_id")]
        public string StoreId { get; set; }
        ///// <summary>
        ///// Názov
        ///// </summary>
        ///// <value>Názov</value>
        //[DataMember(Name = "DisplayName", EmitDefaultValue = false)]
        //[JsonProperty(PropertyName = "DisplayName")]
        //public string DisplayName { get; set; }

        /// <summary>
        /// Vlastné ID [perzistentná položka]
        /// </summary>
        /// <value>Vlastné ID [perzistentná položka]</value>
        [DataMember(Name = "ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }

        /// <summary>
        /// Text [perzistentná položka]
        /// </summary>
        /// <value>Text [perzistentná položka]</value>
        [DataMember(Name = "Text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Text")]
        public string Text { get; set; }

        /// <summary>
        /// Skladová karta; ID objektu Skladová karta [perzistentná položka]
        /// </summary>
        /// <value>Skladová karta; ID objektu Skladová karta [perzistentná položka]</value>
        [DataMember(Name = "StoreCard_ID.Code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "StoreCard_ID.Code")]
        public string StoreCardID_Code { get; set; }

        /// <summary>
        /// Skladová karta; ID objektu Skladová karta [perzistentná položka]
        /// </summary>
        /// <value>Skladová karta; ID objektu Skladová karta [perzistentná položka]</value>
        [DataMember(Name = "StoreCard_ID.Ean", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "StoreCard_ID.Ean")]
        public string StoreCardID_Ean { get; set; }

        /// <summary>
        /// Skladová karta; ID objektu Skladová karta [perzistentná položka]
        /// </summary>
        /// <value>Skladová karta; ID objektu Skladová karta [perzistentná položka]</value>
        [DataMember(Name = "StoreCard_ID.DisplayName", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "StoreCard_ID.DisplayName")]
        public string StoreCardID_DisplayName { get; set; }

        /// <summary>
        /// Skladová karta; ID objektu Skladová karta [perzistentná položka]
        /// </summary>
        /// <value>Skladová karta; ID objektu Skladová karta [perzistentná položka]</value>
        [DataMember(Name = "StoreCard_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "StoreCard_ID")]
        public string StoreCardID { get; set; }

        /// <summary>
        /// Jednotka [perzistentná položka]
        /// </summary>
        /// <value>Jednotka [perzistentná položka]</value>
        [DataMember(Name = "QUnit", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "QUnit")]
        public string QUnit { get; set; }

        /// <summary>
        /// Počet
        /// </summary>
        /// <value>Počet</value>
        [DataMember(Name = "UnitQuantity", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "UnitQuantity")]
        public double? UnitQuantity { get; set; }

        /// <summary>
        /// C.cena [perzistentná položka]
        /// </summary>
        /// <value>C.cena [perzistentná položka]</value>
        [DataMember(Name = "TotalPrice", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "TotalPrice")]
        public double? TotalPrice { get; set; }

        /// <summary>
        /// Poznamka
        /// </summary>
        /// <value>Poznamka</value>
        [DataMember(Name = "X_Note", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_Note")]
        public double? XNote { get; set; }

        [DataMember(Name = "division_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "division_id")]
        public string DivisionId { get; set; }

        [DataMember(Name = "vatrate_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vatrate_id")]
        public string VatRateId { get; set; }
    }
}