﻿using System;
using System.Runtime.CompilerServices;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    internal class AbraWebApiFieldNameAttribute : Attribute
    {
        public AbraWebApiFieldNameAttribute([CallerMemberName] string property = null, string alternativeProperty = null)
        {
            if (!string.IsNullOrEmpty(property))
                PropertyName = property;
            else if (!string.IsNullOrEmpty(alternativeProperty))
                PropertyName = alternativeProperty;
        }

        public string PropertyName { get; }
    }
}