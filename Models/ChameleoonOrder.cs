﻿using Newtonsoft.Json;
using System;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    public class ChameleoonOrder
    {
        /// <summary>
        /// Recipients name
        /// </summary>
        [JsonProperty("addressee")]
        [AbraWebApiFieldName("X_B2C_FirstName")]
        [AbraWebApiFieldName("X_B2C_LastName")]
        public string Addressee { get; set; }

        /// <summary>
        /// Organisation name
        /// </summary>
        [JsonProperty("organizationName")]
        [AbraWebApiFieldName("X_B2C_Firm")]
        public string OrganizationName { get; set; }

        /// <summary>
        /// Street and house number for delivery
        /// </summary>
        [JsonProperty("street")]
        [AbraWebApiFieldName("X_B2C_Street")]
        public string Street { get; set; }

        /// <summary>
        /// Delivery address detail
        /// </summary>
        [JsonProperty("street2")]
        public string Street2 { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [JsonProperty("phone")]
        [AbraWebApiFieldName("X_B2C_PhoneNumber")]
        public string Phone { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [JsonProperty("email")]
        [AbraWebApiFieldName("X_B2C_EMail")]
        public string Email { get; set; }

        /// <summary>
        /// Mesto doručenia
        /// </summary>
        [JsonProperty("city")]
        [AbraWebApiFieldName("X_B2C_City")]
        public string City { get; set; }

        /// <summary>
        /// PSČ
        /// </summary>
        [JsonProperty("zipCode")]
        [AbraWebApiFieldName("X_B2C_PostCode")]
        public string ZipCode { get; set; }

        /// <summary>
        /// Kód krajiny ISO 3166-1 Alpha 2
        /// </summary>
        [JsonProperty("countryCode")]
        [AbraWebApiFieldName("X_B2C_Country")]
        public string CountryCode { get; set; }

        /// <summary>
        /// Poznámka k preprave
        /// </summary>
        [JsonProperty("note")]
        [AbraWebApiFieldName("X_zakaznik_poznamka_zwww")]
        public string Note { get; set; }

        /// <summary>
        /// Povinný údaj - Referencia na záznam v externom systéme
        /// </summary>
        [JsonProperty("referenceNumber", Required = Required.Always)]
        [AbraWebApiFieldName("ID")]
        public string ReferenceNumber { get; set; }

        /// <summary>
        ///  Označenie kuriéra v externom systéme
        /// </summary>
        [JsonProperty("courierService")]
        [AbraWebApiFieldName("TransportationType_ID.Code")]
        public string CourierService { get; set; }

        /// <summary>
        /// Celková váha položiek objednávky
        /// </summary>
        [JsonProperty("weight")]
        [AbraWebApiFieldName("weight")]
        public decimal Weight { get; set; }

        /// <summary>
        /// Počet balíkov ak je známy (pozn. ak nie je známy je lepšie  nechať null)
        /// </summary>
        [JsonProperty("packageCount")]
        public int? PackageCount { get; set; }

        /// <summary>
        /// Hodnota dobierky, ak už bola objednávka uhradená kartou, bank. prevodom alebo iným spôsobom nechať null
        /// </summary>
        [JsonProperty("codPrice")]
        public decimal? CodPrice { get; set; }

        /// <summary>
        /// Stav objednávky v externom systéme
        /// </summary>
        [JsonProperty("stateId")]
        [AbraWebApiFieldName("X_STATUS_OBJEDNAVKY_ID")]
        public string StateId { get; set; }

        /// <summary>
        /// Povinný údaj - Dátum vytvorenia objednávky vo formáte 'yyyy- MM-ddTHH:mm:ss' (napr. 2020-01-31T10:14:13)
        /// </summary>
        [JsonProperty("createDateTime", Required = Required.Always)]
        [AbraWebApiFieldName("CreatedAt$DATE")]
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Id odberného miesta (napr. pri kuriérovi Zásielkovňa alebo DPD pickup point, ...)
        /// </summary>
        [JsonProperty("publicPackagePoint")]
        [AbraWebApiFieldName("X_PICKUP_POINT")]
        public string PublicPackagePoint { get; set; }

        /// <summary>
        /// Príznak či sa jedná o objednávku vrátenia tovaru
        /// </summary>
        [JsonProperty("isBackShipping")]
        public bool IsBackShipping { get; set; }

        /// <summary>
        /// Očakávaný dátum doručenia tovaru, ak je známy  vyplniť vo formáte 'yyyy-MM-ddTHH:mm:ss' (napr. 2020-01-31T10:14:13)
        /// </summary>
        [JsonProperty("orderedShippingDate")]
        public string OrderedShippingDate { get; set; }

        /// <summary>
        /// Povinný údaj - Mena dobierky
        /// </summary>
        [JsonProperty("currency", Required = Required.Always)]
        [AbraWebApiFieldName("Currency_ID.Code")]
        public string Currency { get; set; }

        /// <summary>
        /// Poznámka pre vodiča
        /// </summary>
        [JsonProperty("driverNote")]
        public string DriverNote { get; set; }

        /// <summary>
        /// Kontaktná osoba
        /// </summary>
        [JsonProperty("contactPerson")]
        public string ContactPerson { get; set; }

        /// <summary>
        /// Variabilný symbol - ak nie je vyplnený v systéme sa použije referenčné číslo
        /// </summary>
        [JsonProperty("variableSymbol")]
        [AbraWebApiFieldName("X_ZAKAZNICKE_CISLO_OP")]
        public string VariableSymbol { get; set; }

        /// <summary>
        /// Popis tovaru pre kuriéra
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// "order" | "invoice" | "issue",
        /// </summary>
        [JsonProperty("documentType")]
        public ChameleoonDocType DocumentType { get; set; }

        /// <summary>
        /// Povinný údaj - identifikátor spôsobu úhrady v externom systéme
        /// </summary>
        [JsonProperty("paymentType", Required = Required.Always)]
        [AbraWebApiFieldName("PaymentType_ID.Code")]
        public string PaymentType { get; set; }

        /// <summary>
        /// Povinný údaj - celková suma objednávky
        /// </summary>
        [JsonProperty("totalPrice", Required = Required.Always)]
        public decimal TotalPrice { get; set; }

        [JsonProperty("items")]
        public ChameleoonItem[] Items { get; set; }

    }
}