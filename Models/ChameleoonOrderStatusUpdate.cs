﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    public class ChameleoonOrderStatusUpdate
    {
        /// <summary>
        /// Identifikátor kuriéra v externom systéme, ak sa nezmenil parameter ostane null
        /// </summary>
        [JsonProperty("courier")]
        public string Courier { get; set; }

        /// <summary>
        /// Číslo prepravy pridelené kuriérom
        /// </summary>
        [JsonProperty("shippingNumber")]
        public string ShippingNumber { get; set; }

        /// <summary>
        /// Identifikátor stavu v externom systéme v prípade záujmu o zmenu stavu
        /// </summary>
        [JsonProperty("targetState")]
        public OrderStatus TargetState { get; set; }

        /// <summary>
        /// V pripade ak ma zasielka viac balikov/trackovacich cisel
        /// </summary>
        [JsonProperty("additionalShippingNumbers")]
        public string[] AdditionalShippingNumbers { get; set; }

        [JsonProperty("packageCount")]
        public int PackageCount { get; set; }
    }
}