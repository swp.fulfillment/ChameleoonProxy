﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    [AbraWebApiObjectName("AbraReceivedorderrow")]
    public class ChameleoonItem
    {
        /// <summary>
        /// Povinný údaj - identifikátor položky
        /// </summary>
        [JsonProperty("id", Required = Required.Always)]
        [AbraWebApiFieldName("ID")]
        public string Id { get; set; }

        /// <summary>
        /// Kód produktu
        /// </summary>
        [JsonProperty("code")]
        [AbraWebApiFieldName("StoreCard_ID.Code")]
        public string Code { get; set; }

        /// <summary>
        /// EAN produktu
        /// </summary>
        [JsonProperty("eanCode")]
        [AbraWebApiFieldName("StoreCard_ID.Ean")]
        public string EanCode { get; set; }

        /// <summary>
        /// Povinný údaj - Názov produktu
        /// </summary>
        [JsonProperty("name", Required = Required.Always)]
        [AbraWebApiFieldName("StoreCard_ID.DisplayName")]
        public string Name { get; set; }

        /// <summary>
        /// Názov položky
        /// </summary>
        [JsonProperty("text")]
        [AbraWebApiFieldName]
        public string Text { get; set; }

        /// <summary>
        /// Povinný údaj - množstvo
        /// </summary>
        [JsonProperty("quantity", Required = Required.Always)]
        [AbraWebApiFieldName("UnitQuantity")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// Merna jednotka
        /// </summary>
        [JsonProperty("measureUnit")]
        [AbraWebApiFieldName("QUnit")]
        public string MeasureUnit { get; set; }
    }
}