﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SwP.Fulfillment.ChameleoonProxy.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class AbraReceivedorder
    {
        /// <summary>
        /// Číslo dok.
        /// </summary>
        /// <value>Číslo dok.</value>
        [DataMember(Name = "DisplayName", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "DisplayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Vlastné ID [perzistentná položka]
        /// </summary>
        /// <value>Vlastné ID [perzistentná položka]</value>
        [DataMember(Name = "ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }

        /// <summary>
        /// Riadky; kolekcia BO Objednávka prijatá - riadok [neperzistentná položka]
        /// </summary>
        /// <value>Riadky; kolekcia BO Objednávka prijatá - riadok [neperzistentná položka]</value>
        [DataMember(Name = "Rows", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Rows")]
        public List<AbraReceivedorderrow> Rows { get; set; }

        /// <summary>
        /// Dátum dok. [perzistentná položka]
        /// </summary>
        /// <value>Dátum dok. [perzistentná položka]</value>
        [DataMember(Name = "DocDate$DATE", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "DocDate$DATE")]
        public DateTime? DocDateDATE { get; set; }

        /// <summary>
        /// Opravené [perzistentná položka]
        /// </summary>
        /// <value>Opravené [perzistentná položka]</value>
        [DataMember(Name = "CorrectedAt$DATE", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "CorrectedAt$DATE")]
        public DateTime? CorrectedAtDATE { get; set; }

        /// <summary>
        /// Firma; ID objektu Firma [perzistentná položka]
        /// </summary>
        /// <value>Firma; ID objektu Firma [perzistentná položka]</value>
        [DataMember(Name = "Firm_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Firm_ID")]
        public string FirmID { get; set; }

        /// <summary>
        /// Firma; ID objektu Firma [perzistentná položka]
        /// </summary>
        /// <value>Firma; ID objektu Firma [perzistentná položka]</value>
        [DataMember(Name = "Firm_ID.Name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Firm_ID.Name")]
        public string FirmID_Name { get; set; }

        /// <summary>
        /// Popis [perzistentná položka]
        /// </summary>
        /// <value>Popis [perzistentná položka]</value>
        [DataMember(Name = "Description", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Krajina určenia; ID objektu Krajina [perzistentná položka]
        /// </summary>
        /// <value>Krajina určenia; ID objektu Krajina [perzistentná položka]</value>
        [DataMember(Name = "Country_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Country_ID")]
        public string CountryID { get; set; }

        /// <summary>
        /// Mena; ID objektu Mena [perzistentná položka]
        /// </summary>
        /// <value>Mena; ID objektu Mena [perzistentná položka]</value>
        [DataMember(Name = "Currency_ID.Code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Currency_ID.Code")]
        public string CurrencyID_Code { get; set; }

        /// <summary>
        /// Mena; ID objektu Mena [perzistentná položka]
        /// </summary>
        /// <value>Mena; ID objektu Mena [perzistentná položka]</value>
        [DataMember(Name = "Currency_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Currency_ID")]
        public string CurrencyID { get; set; }

        /// <summary>
        /// Celk.hmot.
        /// </summary>
        /// <value>Celk.hmot.</value>
        [DataMember(Name = "Weight", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "Weight")]
        public double? Weight { get; set; }

        ///// <summary>
        ///// Jedn.celk.hm.
        ///// </summary>
        ///// <value>Jedn.celk.hm.</value>
        //[DataMember(Name = "WeightUnit", EmitDefaultValue = false)]
        //[JsonProperty(PropertyName = "WeightUnit")]
        //public int? WeightUnit { get; set; }

        /// <summary>
        /// Typ dopr.; ID objektu Spôsob dopravy [perzistentná položka]
        /// </summary>
        /// <value>Typ dopr.; ID objektu Spôsob dopravy [perzistentná položka]</value>
        [DataMember(Name = "TransportationType_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "TransportationType_ID")]
        public string TransportationTypeID { get; set; }

        /// <summary>
        /// Typ dopr.; ID objektu Spôsob dopravy [perzistentná položka]
        /// </summary>
        /// <value>Typ dopr.; ID objektu Spôsob dopravy [perzistentná položka]</value>
        [DataMember(Name = "TransportationType_ID.Code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "TransportationType_ID.Code")]
        public string TransportationTypeID_Code { get; set; }

        /// <summary>
        /// Typ úhr.; ID objektu Spôsob úhrady [perzistentná položka]
        /// </summary>
        /// <value>Typ úhr.; ID objektu Spôsob úhrady [perzistentná položka]</value>
        [DataMember(Name = "PaymentType_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "PaymentType_ID")]
        public string PaymentTypeID { get; set; }

        /// <summary>
        /// Typ úhr.; ID objektu Spôsob úhrady [perzistentná položka]
        /// </summary>
        /// <value>Typ úhr.; ID objektu Spôsob úhrady [perzistentná položka]</value>
        [DataMember(Name = "PaymentType_ID.Code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "PaymentType_ID.Code")]
        public string PaymentTypeID_Code { get; set; }

        /// <summary>
        /// Externé číslo [perzistentná položka]
        /// </summary>
        /// <value>Externé číslo [perzistentná položka]</value>
        [DataMember(Name = "ExternalNumber", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ExternalNumber")]
        public string ExternalNumber { get; set; }

        /// <summary>
        /// Vytvorené [perzistentná položka]
        /// </summary>
        /// <value>Vytvorené [perzistentná položka]</value>
        [DataMember(Name = "CreatedAt$DATE", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "CreatedAt$DATE")]
        public DateTime? CreatedAtDATE { get; set; }

        /// <summary>
        /// Reference URL [perzistentná položka]
        /// </summary>
        /// <value>Reference URL [perzistentná položka]</value>
        [DataMember(Name = "X_URL", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_URL")]
        public string X_URL { get; set; }

        /// <summary>
        /// E-mail [perzistentná položka]
        /// </summary>
        /// <value>E-mail [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_EMail", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_EMail")]
        public string XB2CEMail { get; set; }

        /// <summary>
        /// PSČ [perzistentná položka]
        /// </summary>
        /// <value>PSČ [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_PostCode", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_PostCode")]
        public string XB2CPostCode { get; set; }

        /// <summary>
        /// Telefón [perzistentná položka]
        /// </summary>
        /// <value>Telefón [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_PhoneNumber", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_PhoneNumber")]
        public string XB2CPhoneNumber { get; set; }

        /// <summary>
        /// Poznamka_zakaz_naO_zWWW [perzistentná položka]
        /// </summary>
        /// <value>Poznamka_zakaz_naO_zWWW [perzistentná položka]</value>
        [DataMember(Name = "X_zakaznik_poznamka_zwww", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_zakaznik_poznamka_zwww")]
        public string XZakaznikPoznamkaZwww { get; set; }

        /// <summary>
        /// Status objednávky; ID objektu Status objednávky [perzistentná položka]
        /// </summary>
        /// <value>Status objednávky; ID objektu Status objednávky [perzistentná položka]</value>
        [DataMember(Name = "X_STATUS_OBJEDNAVKY_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_STATUS_OBJEDNAVKY_ID")]
        public string X_STATUS_OBJEDNAVKY_ID { get; set; }

        /// <summary>
        /// Krajina [perzistentná položka]
        /// </summary>
        /// <value>Krajina [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_Country", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_Country")]
        public string XB2CCountry { get; set; }

        /// <summary>
        /// Krajina [perzistentná položka]
        /// </summary>
        /// <value>Krajina [perzistentná položka]</value>
        [DataMember(Name = "X_COUNTRY_ISO_CODE", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_COUNTRY_ISO_CODE")]
        public string XCountryIsoCode { get; set; }

        /// <summary>
        /// Tracking ID objednávky [perzistentná položka]
        /// </summary>
        /// <value>Tracking ID objednávky [perzistentná položka]</value>
        [DataMember(Name = "X_TRACKING_ID", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_TRACKING_ID")]
        public string X_TRACKING_ID { get; set; }

        /// <summary>
        /// Firma [perzistentná položka]
        /// </summary>
        /// <value>Firma [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_Firm", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_Firm")]
        public string XB2CFirm { get; set; }

        /// <summary>
        /// Zákaznícke číslo OP [perzistentná položka]
        /// </summary>
        /// <value>Zákaznícke číslo OP [perzistentná položka]</value>
        [DataMember(Name = "X_ZAKAZNICKE_CISLO_OP", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_ZAKAZNICKE_CISLO_OP")]
        public string X_ZAKAZNICKE_CISLO_OP { get; set; }

        /// <summary>
        /// Pickup point (ZPOINT)  [perzistentná položka]
        /// </summary>
        /// <value>Pickup point (ZPOINT)  [perzistentná položka]</value>
        [DataMember(Name = "X_PICKUP_POINT", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_PICKUP_POINT")]
        public string X_PICKUP_POINT { get; set; }

        /// <summary>
        /// Mesto [perzistentná položka]
        /// </summary>
        /// <value>Mesto [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_City", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_City")]
        public string XB2CCity { get; set; }

        /// <summary>
        /// Ulica [perzistentná položka]
        /// </summary>
        /// <value>Ulica [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_Street", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_Street")]
        public string XB2CStreet { get; set; }

        /// <summary>
        /// Ulica [perzistentná položka]
        /// </summary>
        /// <value>Ulica [perzistentná položka]</value>
        [DataMember(Name = "X_ADDRESS_LINE_2", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_ADDRESS_LINE_2")]
        public string XAddressLine2 { get; set; }

        /// <summary>
        /// Meno [perzistentná položka]
        /// </summary>
        /// <value>Meno [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_FirstName", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_FirstName")]
        public string XB2CFirstName { get; set; }

        /// <summary>
        /// Priezvisko [perzistentná položka]
        /// </summary>
        /// <value>Priezvisko [perzistentná položka]</value>
        [DataMember(Name = "X_B2C_LastName", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "X_B2C_LastName")]
        public string XB2CLastName { get; set; }

        [DataMember(Name = "docqueue_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "docqueue_id")]
        public string DocQueueID { get; set; }

        [DataMember(Name = "period_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "period_id")]
        public string PrediodID { get; set; }
    }
}