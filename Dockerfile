#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /src
COPY ["SwP.Fulfillment.BusinessObjects/Swp.Fulfillment.BusinessObjects.csproj", "SwP.Fulfillment.BusinessObjects/"]
COPY ["SwP.Fulfillment.Chameleoon.API/SwP.Fulfillment.Chameleoon.API.csproj", "SwP.Fulfillment.Chameleoon.API/"]
RUN dotnet restore "SwP.Fulfillment.Chameleoon.API/SwP.Fulfillment.Chameleoon.API.csproj"
COPY . .

RUN dotnet build "SwP.Fulfillment.Chameleoon.API/SwP.Fulfillment.Chameleoon.API.csproj" -c Release -o /app/build

FROM build AS publish
WORKDIR "/src/SwP.Fulfillment.Chameleoon.API"
RUN dotnet publish "SwP.Fulfillment.Chameleoon.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SwP.Fulfillment.Chameleoon.API.dll"]

#docker build -f Dockerfile --force-rm -t luptak/swpfulfillmentchameleoonapi:v3.0  --label "com.microsoft.created-by=visual-studio" --label "com.microsoft.visual-studio.project-name=SwP.Fulfillment.Chameleoon.API" .

#FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
#WORKDIR /src
#COPY ["SwP.Fulfillment.BusinessObjects/Swp.Fulfillment.BusinessObjects.csproj", "SwP.Fulfillment.BusinessObjects/"]
#COPY ["SwP.Fulfillment.Abra.API/SwP.Fulfillment.Abra.API.csproj", "SwP.Fulfillment.Abra.API/"]
#RUN dotnet restore "SwP.Fulfillment.Abra.API/SwP.Fulfillment.Abra.API.csproj"
#COPY . .
#
#RUN dotnet build "SwP.Fulfillment.Abra.API/SwP.Fulfillment.Abra.API.csproj" -c Release -o /app/build
#
#FROM build AS publish
#WORKDIR "/src/SwP.Fulfillment.Abra.API"
#RUN dotnet publish "SwP.Fulfillment.Abra.API.csproj" -c Release -o /app/publish
#
#FROM base AS final
#WORKDIR /app
#COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "SwP.Fulfillment.Abra.API.dll"]

#docker build -f Dockerfile --force-rm -t luptak/swpfulfillmentabraapi:v1.0.0  --label "com.microsoft.created-by=visual-studio" --label "com.microsoft.visual-studio.project-name=SwP.Fulfillment.Abra.API" .
