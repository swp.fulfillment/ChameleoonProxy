﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Net.Http;
using SwP.Fulfillment.ChameleoonProxy.Services;
using SwP.Fulfillment.ChameleoonProxy.Models;

namespace SwP.Fulfillment.ChameleoonProxy.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]/{clientId}")]
    public class OrdersController : ControllerBase
    {
        private readonly IChameleoonService _abraWmsService;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(ILogger<OrdersController> logger, IChameleoonService abraWmsService)
        {
            _abraWmsService = abraWmsService;
            _logger = logger;
        }

        /// <summary>Fetch order</summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpGet("{orderId}")]
        public async Task<ActionResult> Get(string clientId, string orderId)
        {
            _logger.LogDebug("Request for order received");
            try
            {
                var result = await _abraWmsService.Get(clientId, orderId);

                if (result == null) return NotFound();

                return Ok(result);
            }
            catch (System.Exception e)
            {
                _logger.LogError(e, "Captured exception GET(id)");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        /// <summary>Update order state</summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="orderState">State of the order.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpPut("{orderId}")]
        public async Task<ActionResult<HttpResponseMessage>> Put(string clientId, string orderId, [FromBody] ChameleoonOrderStatusUpdate orderState)
        {
            _logger.LogDebug($"Request to change state of order {orderId} received");
            try
            {
                var result = await _abraWmsService.Patch(clientId, orderId, orderState);

                if (result == null)
                    return NotFound();
                if (result.IsSuccessStatusCode)
                    return StatusCode(StatusCodes.Status204NoContent);

                return BadRequest("Response from server behind: " + result.ReasonPhrase); ;

            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error updating data");
                //return new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError)
            }
        }
        /// <summary>Fetch multiple orders</summary>
        /// <param name="clientId"></param>
        /// <param name="state"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Get(string clientId, [FromQuery] OrderStatus? state, [FromQuery] System.DateTime? date_from = null, [FromQuery] System.DateTime? date_to = null)
        {
            _logger.LogDebug("Request for order listing received");
            try
            {
                return Ok(await _abraWmsService.Get(clientId, state, date_from, date_to));
            }
            catch (System.Exception e)
            {
                _logger.LogError(e, "Captured exception GET");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }
    }

}
