﻿//this was used only for debuging (to find out what is chameleoon sending back)
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SwP.Fulfillment.ChameleoonProxy.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]/{clientId}")]
    public class ResponseTestController : ControllerBase
    {
        private readonly ILogger<ResponseTestController> _logger;

        public ResponseTestController(ILogger<ResponseTestController> logger)
        {
            _logger = logger;
        }

        [HttpPut("{orderId}")]
        public async Task<ActionResult<HttpResponseMessage>> Put(string clientId, string orderId, [FromBody] JsonElement json)
        {
            _logger.LogDebug("Request to change state of order {0} received", json.GetRawText());
            return await Task.FromResult(StatusCode(StatusCodes.Status204NoContent));
        }

    }
}
