﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SwP.Fulfillment.ChameleoonProxy.Models;
using SwP.Fulfillment.ChameleoonProxy.Models.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwP.Fulfillment.ChameleoonProxy.Services
{
    public interface IUserService
    {
        Task<User> Authenticate(string username, string password);

        Task<IEnumerable<User>> GetAll();
    }

    public class UserService : IUserService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<IUserService> _logger;

        public UserService(IConfiguration configuration, ILogger<IUserService> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            //_logger.LogDebug("Requesting users details");

            var users = _configuration.GetSection("DestApiUsers").Get<User[]>();
            //_logger.LogDebug("{0} users found", users.Count());

            var user = await Task.Run(() => users.SingleOrDefault(x => x.Username == username && x.Password == password));

            // return null if user not found
            if (user == null)
            {
                _logger.LogError($"Authentication for user {username} failed");
                return null;
            }
            // authentication successful so return user details without password
            _logger.LogDebug($"User {username} authenticated");
            return user.WithoutPassword();
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            var users = _configuration.GetSection("DestApiUsers").Get<User[]>();
            return await Task.Run(() => users.WithoutPasswords());
        }
    }

}
