﻿using System.Net.Http;
using System.Threading.Tasks;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using SwP.Fulfillment.ChameleoonProxy.Models;
using SwP.Fulfillment.ChameleoonProxy.Models.Extensions;

namespace SwP.Fulfillment.ChameleoonProxy.Services
{
    public interface IChameleoonService
    {
        //Task<Order> GetOrder(string orderId); <- disabled for now
        Task<ChameleoonOrder> Get(string clientId, string orderId);

        Task<ChameleoonOrder[]> Get(string clientId, OrderStatus? orderStatus, DateTime? dateFrom, DateTime? dateTo);

        Task<HttpResponseMessage> Patch(string clientId, string orderId, ChameleoonOrderStatusUpdate orderState);
    }
    public class ChameleoonService : BaseAbraWmsService, IChameleoonService
    {
        /// <summary>The logger</summary>
        private readonly ILogger<ChameleoonService> _logger;

        public ChameleoonService(IConfiguration configuration, ILogger<ChameleoonService> logger) : base(configuration)
        {
            _logger = logger;
        }
        /// <summary>
        /// Get endpoint.
        /// Allows to retrieve list of orders meeting given criteria
        /// </summary>
        /// <param name="clientCode">Client code in ABRA</param>
        /// <param name="state">Order state in ABRA</param>
        /// <param name="dateFrom">Min. order date</param>
        /// <param name="dateTo">Max. order date</param>
        /// <returns>List of orders</returns>
        public async Task<ChameleoonOrder[]> Get(string clientCode, OrderStatus? state, DateTime? dateFrom, DateTime? dateTo)
        {
            state ??= _configuration.GetSection("RetrieveOrderState").Get<OrderStatus>();

            var select = QueryParameters.ComposeSelectWithRowsFromAbraObject<AbraReceivedorder, AbraReceivedorderrow>();

            var requestUri = $"receivedorders";
            var hardwiredFilter = $"?where=firm_id+eq+'{clientCode}'+and+x_status_objednavky_id+eq+'{state?.ToCode()}'";

            if (dateFrom != null && dateTo != null)
            {
                requestUri += hardwiredFilter + $"+and+CreatedAt$DATE+ge+timestamp'{dateFrom:yyyy-MM-dd}'+and+CreatedAt$DATE+le+timestamp'{dateTo:yyyy-MM-dd}'&";
            }
            else if (dateFrom != null)
            {
                requestUri += hardwiredFilter + $"+and+CreatedAt$DATE+ge+timestamp'{dateFrom:yyyy-MM-dd}'&";
                //requestUri += $"?where=CreatedAt$DATE+ge+timestamp'{dateFrom:yyyy-MM-dd}'&";
            }
            else if (dateTo != null)
            {
                requestUri += hardwiredFilter + $"+and+CreatedAt$DATE+le+timestamp'{dateTo:yyyy-MM-dd}'&";
                //requestUri += $"?where=CreatedAt$DATE+le+timestamp'{dateTo:yyyy-MM-dd}'&";
            }
            else
            {
                requestUri += hardwiredFilter + $"&";
                //requestUri += $"?";
            }
            requestUri += select;

            HttpResponseMessage response = await _client.GetAsync(requestUri);
            if (response.IsSuccessStatusCode)
            {
                var orders = await response.Content.ReadAsAsync<AbraReceivedorder[]>();
                return (from o in orders
                        let cho = new ChameleoonOrder()
                        select cho.FromAbraReceivedorder(o)).ToArray();
            }
            else
            {
                _logger.LogError("Request to ABRA failed. Server replied with " + response.StatusCode);
            }
            return null;
        }
        /// <summary>
        /// Get endpont.
        /// Returns single order
        /// </summary>
        /// <param name="clientCode">Client code in ABRA</param>
        /// <param name="orderId">Order ID number in ABRA</param>
        /// <returns>Single order</returns>
        public async Task<ChameleoonOrder> Get(string clientCode, string orderId)
        {
            //var select = "select=" + ComposeQuery<AbraReceivedorder>();
            var select = QueryParameters.ComposeSelectWithRowsFromAbraObject<AbraReceivedorder, AbraReceivedorderrow>();
            //var requestUri = $"receivedorders/{orderId}?where=x_status_objednavky_id+in+('{OrderStatus.WaitingForPickup.ToCode()}','{OrderStatus.Sent.ToCode()}','{OrderStatus.Picking.ToCode()}')&{select}";
            var requestUri = $"receivedorders/{orderId}?{select}";
            HttpResponseMessage response = await _client.GetAsync(requestUri);
            if (response.IsSuccessStatusCode)
            {
                var order = await response.Content.ReadAsAsync<AbraReceivedorder>();
                if (order.FirmID != clientCode)
                {
                    _logger.LogWarning($"Missmatch between selected cliendId({clientCode}) and client id in response({order.FirmID})");
                    return null;
                }
                if (order.X_STATUS_OBJEDNAVKY_ID != OrderStatus.WaitingForPickup.ToCode() && order.X_STATUS_OBJEDNAVKY_ID != OrderStatus.Sent.ToCode() && order.X_STATUS_OBJEDNAVKY_ID != OrderStatus.Picking.ToCode())
                {
                    _logger.LogWarning($"Order in this state cannot be processed (Current state: {order.X_STATUS_OBJEDNAVKY_ID})");
                    return null;

                }
                if (order.X_STATUS_OBJEDNAVKY_ID != OrderStatus.Picking.ToCode())
                {
                    try
                    {
                        var json = "{\"X_STATUS_OBJEDNAVKY_ID\":\"" + OrderStatus.Picking.ToCode() + "\"}";
                        _logger.LogDebug("Sending patch request : " + json);

                        var httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage patchResponse = await _client.PutAsync("receivedorders/" + orderId, httpContent);
                        if (patchResponse.IsSuccessStatusCode)
                        {
                            _logger.LogDebug("Successfully changed state to 'Picking'");
                        }
                        else
                        {
                            _logger.LogError("Update request failed. Server replied with " + response.StatusCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Failed to change order state");
                        _logger.LogError(ex.Message);
                    }
                }
                var cho = new ChameleoonOrder();
                return cho.FromAbraReceivedorder(order);
            }
            else
            {
                _logger.LogError("Request to ABRA failed. Server replied with " + response.StatusCode);
            }
            return null;
        }
        /// <summary>
        /// Patch endpoint.
        /// Allows to change state of order
        /// </summary>
        /// <param name="cliendId">Client code in ABRA</param>
        /// <param name="orderId">Order ID number in ABRA</param>
        /// <param name="orderState">New order state</param>
        /// <returns>Response from ABRA api</returns>
        public async Task<HttpResponseMessage> Patch(string cliendId, string orderId, ChameleoonOrderStatusUpdate orderState)
        {
            var abraOrderState = orderState.ToAbraReceivedorderSimplified();

            var jsonWriter = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            var json = JsonConvert.SerializeObject(abraOrderState, Formatting.None, jsonWriter);

            _logger.LogDebug("Sending patch request : " + json);
            var httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PutAsync("receivedorders/" + orderId, httpContent);

            return response;
        }
    }
}